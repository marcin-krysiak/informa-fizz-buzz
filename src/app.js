// Step 3: Enhance your existing solution to perform the following:
//
//   Produce a report at the end of the program showing how many times the following were output ** fizz ** buzz ** fizzbuzz ** lucky ** an integer
// e.g. if I run the program over a range from 1-20 I should get the following output
//
// 1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz fizz: 4 buzz: 3 fizzbuzz: 1 lucky: 2 integer: 10
//
// (Integer is 10 because there were 10 numbers that were not altered in any way).

window.fizzBuzz = range => {
  if (typeof range !== 'number') {
    return false;
  }

  const result = [...Array(range).keys()]
    .map((item, index, array) => {
      const number = index + 1;

      return array[index] =
        ((number.toString(10).includes('3')) && ('lucky')) || // If the number contains a three you must output the text 'lucky'
        ((number % 15 === 0) && ('fizzbuzz')) || // 'fizzbuzz' for numbers that are multiples of 15
        ((number % 5 === 0) && ('buzz')) || // 'buzz' for numbers that are multiples of 5
        ((number % 3 === 0) && ('fizz')) || // 'fizz' for numbers that are multiples of 3
        number; // the number
    });

  let output = result.join(' ');

  // Produce a report at the end of the program showing how many times the following were output ** fizz ** buzz ** fizzbuzz ** lucky ** an integer
  ['fizz', 'buzz', 'fizzbuzz', 'lucky']
    .forEach(item => output += `\n${item}: ${result.filter(filteredItem => filteredItem === item).length}`);
  output += `\ninteger: ${result.filter(item => typeof item === 'number').length}`;

  return output;
};
