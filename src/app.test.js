import './app';

describe('Fizz Buzz - step 1', () => {
  it('should be global method', () => {
    expect(window.fizzBuzz).toBeTruthy();
  });

  it('should print out numbers, fizz, buzz, fizzbuzz and per test description', () => {
    expect(window.fizzBuzz(20)).toBe('1 2 lucky 4 buzz fizz 7 8 fizz buzz 11 fizz lucky 14 fizzbuzz 16 17 fizz 19 buzz fizz: 4 buzz: 3 fizzbuzz: 1 lucky: 2 integer: 10');
  })

  it('should return false if range is not a number', () => {
    expect(window.fizzBuzz('')).toBeFalsy();
    expect(window.fizzBuzz(true)).toBeFalsy();
    expect(window.fizzBuzz({})).toBeFalsy();
    expect(window.fizzBuzz([])).toBeFalsy();
    expect(window.fizzBuzz()).toBeFalsy();
  })
});
