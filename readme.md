# Fizz Buzz app #

## installation ##
* `yarn`

## running the app ##
* `yarn start` (works for mac only. For PC or windows open ./src/index.html in your browser manually)
* open console
* `fizzBuzz(<number>)`

## running tests ##
* `yarn test`

## accessing the project sourcecode ##
* public git repository = `https://gitlab.com/marcin.krysiak/informa-fizz-buzz`